export interface TopicsConfigInterface {
  topics: {
    internal: string[];
    external: string[];
    gpu?: string[];
  };
}
