export type LOG_LEVEL =
  | "fatal"
  | "error"
  | "warn"
  | "info"
  | "debug"
  | "trace"
  | "verbose";

export const DEFAULT_LOG_LEVELS: { [key in LOG_LEVEL]: number } = {
  trace: 10,
  verbose: 10,
  debug: 20,
  info: 30,
  warn: 40,
  error: 50,
  fatal: 60,
};
