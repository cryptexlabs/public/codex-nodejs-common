export interface TelemetryConfigInterface {
  metrics: MetricsConfig;
  traces: TracesConfig;
}

export interface MetricsConfig {
  enabled: boolean;
  port: number;
  path: string;
}

export interface TracesConfig {
  enabled: boolean;
  traceUrl: string;
}
