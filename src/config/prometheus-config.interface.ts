export interface PrometheusConfigInterface {
  enabled: boolean;
  serverPrefix: string;
  config: {
    host: string;
    port: number;
  };
}
