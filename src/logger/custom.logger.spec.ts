import { CustomLogger } from "./custom.logger";
import { Logger } from "@nestjs/common";
import { deepEqual, instance, mock, verify } from "ts-mockito";

describe("CustomLogger", () => {
  let loggerMock: Logger;
  let mockedLogger: Logger;

  beforeEach(() => {
    mockedLogger = mock(Logger);
    loggerMock = instance(mockedLogger);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("Should allow the default context when no filtered contexts are provided", () => {
    const customLogger = new CustomLogger("info", [], true, loggerMock);
    customLogger.info("hello");
    verify(mockedLogger.log("hello", "info")).once();
  });

  it("Should allow the default context allowed filtered contexts is provided", () => {
    const customLogger = new CustomLogger("info", ["info"], true, loggerMock);
    customLogger.info("hello");
    verify(mockedLogger.log("hello", "info")).once();
  });

  it("Should now allow the default context when filtered contexts does not include default context", () => {
    const customLogger = new CustomLogger("info", ["debug"], true, loggerMock);
    customLogger.log("hello");
    verify(mockedLogger.log("hello", "info")).never();
  });

  it("Should not allow custom context when filtered contexts does not include custom context", () => {
    const customLogger = new CustomLogger("info", ["debug"], true, loggerMock);
    customLogger.log("hello", "error");
    verify(mockedLogger.error("hello")).never();
  });

  it("Should not allow error with a method when filtered contexts does not include error", () => {
    const customLogger = new CustomLogger("info", ["debug"], true, loggerMock);
    customLogger.error("hello", "", "");
    verify(mockedLogger.error("hello", "")).never();
  });

  it("Should not allow debug with a method when filtered contexts does not include debug", () => {
    const customLogger = new CustomLogger("info", ["info"], true, loggerMock);
    customLogger.debug("hello");
    verify(mockedLogger.debug("hello")).never();
  });

  it("Should not allow info with a method when filtered contexts does not include info", () => {
    const customLogger = new CustomLogger("info", ["debug"], true, loggerMock);
    customLogger.info("hello");
    verify(mockedLogger.log("hello", "info")).never();
  });

  it("Should not allow verbose with a method when filtered contexts does not include verbose", () => {
    const customLogger = new CustomLogger("info", ["info"], true, loggerMock);
    customLogger.verbose("hello");
    verify(mockedLogger.verbose("hello")).never();
  });

  it("Should not allow warn with a method when filtered contexts does not include warn", () => {
    const customLogger = new CustomLogger("info", ["info"], true, loggerMock);
    customLogger.warn("hello");
    verify(mockedLogger.warn("hello")).never();
  });

  it("Should allow debug with a method when filtered contexts does not include debug", () => {
    const customLogger = new CustomLogger(
      "info",
      ["debug", "info"],
      true,
      loggerMock
    );
    customLogger.debug("hello");
    verify(mockedLogger.debug("hello")).once();
  });

  it("Should allow error when error is in filtered context", () => {
    const customLogger = new CustomLogger("info", ["error"], true, loggerMock);
    customLogger.error("hello");
    verify(mockedLogger.error("hello")).once();
  });

  it("Should send custom options with debug level", () => {
    const customLogger = new CustomLogger("info", ["debug"], true, loggerMock);
    customLogger.debug("hello", { world: "gone" });
    verify(mockedLogger.debug("hello", deepEqual({ world: "gone" }))).once();
  });

  it("Should send custom options with error level", () => {
    const customLogger = new CustomLogger("info", ["error"], true, loggerMock);
    customLogger.error("hello", { world: "gone" });
    verify(mockedLogger.error("hello", deepEqual({ world: "gone" }))).once();
  });

  it("Should send custom options with verbose level", () => {
    const customLogger = new CustomLogger(
      "info",
      ["verbose"],
      true,
      loggerMock
    );
    customLogger.verbose("hello", { world: "gone" });
    verify(mockedLogger.verbose("hello", deepEqual({ world: "gone" }))).once();
  });

  it("Should send custom options with warn level", () => {
    const customLogger = new CustomLogger("info", ["warn"], true, loggerMock);
    customLogger.warn("hello", { world: "gone" });
    verify(mockedLogger.warn("hello", deepEqual({ world: "gone" }))).once();
  });
});
