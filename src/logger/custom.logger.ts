import { Injectable, Logger, LoggerService } from "@nestjs/common";

@Injectable()
export class CustomLogger implements LoggerService {
  private readonly logger: LoggerService;
  private readonly filterContext: string[];

  constructor(
    private defaultContext: string = "info",
    filterContext: string[] = [],
    isTimestampEnabled?: boolean,
    logger?: LoggerService
  ) {
    this.filterContext = filterContext;

    if (logger) {
      this.logger = logger;
    } else {
      this.logger = new Logger(defaultContext, {
        timestamp: isTimestampEnabled,
      });
    }
  }

  public log(message: string, ...optionalParams: any) {
    if (this.filterContext.length > 0) {
      if (this.filterContext.includes("log")) {
        this.logger.log(message, ...optionalParams);
      }
    } else {
      this.logger.log(message, ...optionalParams);
    }
  }

  public error(error: string | Error, ...optionalParams: any) {
    let message;
    let useTrace;
    if (error instanceof Error) {
      message = error.message;
      useTrace = error.stack;
    } else if (typeof error === "object" && (error as any).stack) {
      message = error;
      useTrace = (error as any).stack;
    }
    if (!message && ((error as any).message as any)) {
      message = (error as any).message;
    }
    if (!useTrace && ((error as any).stack as any)) {
      useTrace = (error as any).stack;
    }
    if (!message && typeof error === "string") {
      message = error;
    }

    if (this.filterContext.length > 0) {
      if (this.filterContext.includes("error")) {
        if (useTrace) {
          this.logger.error(message, { trace: useTrace, ...optionalParams });
        } else {
          this.logger.error(message, ...optionalParams);
        }
      }
    } else {
      if (useTrace) {
        this.logger.error(message, { trace: useTrace, ...optionalParams });
      } else {
        this.logger.error(message, ...optionalParams);
      }
    }
  }

  public debug(message: any, ...optionalParams: any) {
    if (this.filterContext.length > 0) {
      if (this.filterContext.includes("debug")) {
        this.logger.debug(message, ...optionalParams);
      }
    } else {
      this.logger.debug(message, ...optionalParams);
    }
  }

  public verbose(message: any, ...optionalParams: any): any {
    if (this.filterContext.length > 0) {
      if (this.filterContext.includes("verbose")) {
        this.logger.verbose(message, ...optionalParams);
      }
    } else {
      this.logger.verbose(message, ...optionalParams);
    }
  }

  public warn(message: any, ...optionalParams: any): any {
    if (this.filterContext.length > 0) {
      if (this.filterContext.includes("warn")) {
        this.logger.warn(message, ...optionalParams);
      }
    } else {
      this.logger.warn(message, ...optionalParams);
    }
  }

  public info(message: any): any {
    if (this.filterContext.length > 0) {
      if (this.filterContext.includes("info")) {
        this.logger.log(message, "info");
      }
    } else {
      this.logger.log(message, "info");
    }
  }
}
