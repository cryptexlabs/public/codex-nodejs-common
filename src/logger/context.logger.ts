import { LoggerService } from "@nestjs/common";
import { v4 as uuidV4 } from "uuid";
import { DefaultConfig } from "../config";
import { ClientInterface } from "@cryptexlabs/codex-data-model";
import { CustomLogger } from "./custom.logger";

export class ContextLogger implements LoggerService {
  private readonly stackTraceId: string;

  constructor(
    private readonly correlationId,
    private readonly config: DefaultConfig,
    private readonly client: ClientInterface,
    private readonly logger: LoggerService
  ) {
    this.stackTraceId = uuidV4();
  }

  public debug(message: any, data?: any): any {
    this.logger.debug(message, this._getData(data));
  }

  public error(message: any, data?: any): any {
    this.logger.error(message, this._getData(data));
  }

  public log(message: any, data?: any): any {
    this.logger.log(message, this._getData(data));
  }

  public verbose(message: any, data?: any): any {
    this.logger.verbose(message, this._getData(data));
  }

  public warn(message: any, data?: any): any {
    this.logger.warn(message, this._getData(data));
  }

  private _getData(data?: any) {
    let logData: any = {
      stackTraceId: this.stackTraceId,
      correlationId: this.correlationId,
      appVersion: this.config.appVersion,
      appName: this.config.appName,
      clientName: this.client.name,
      clientVersion: this.client.version,
      clientVariant: this.client.variant,
    };

    if (data) {
      logData = {
        ...logData,
        data,
      };
    }
    return logData;
  }
}
