import { ClientInterface } from "@cryptexlabs/codex-data-model";

export class Client implements ClientInterface {
  constructor(
    public readonly id: string,
    public readonly version: string,
    public readonly name: string,
    public readonly variant: string
  ) {}
}
