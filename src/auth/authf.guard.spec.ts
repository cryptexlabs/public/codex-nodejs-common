import { AuthfGuard } from "./authf.guard";
import { DefaultConfig } from "../config";
import { instance, mock, when } from "ts-mockito";
import { Context, ContextBuilder } from "../context";

describe("AuthGuard", () => {
  let authGuard: AuthfGuard;
  let ConfigMock: DefaultConfig;
  let ContextBuilderMock: ContextBuilder;
  let ContextMock: Context;

  beforeEach(() => {
    ConfigMock = mock(DefaultConfig);
    ContextBuilderMock = mock(ContextBuilder);
    ContextMock = mock(Context);

    when(ConfigMock.authEnabled).thenReturn(false);
    when(ContextBuilderMock.build()).thenReturn(instance(ContextBuilderMock));
    when(ContextBuilderMock.getResult()).thenReturn(instance(ContextMock));
    when(ContextMock.logger).thenReturn({
      debug: () => {},
      warn: () => {},
    } as any);

    authGuard = new AuthfGuard(
      instance(ConfigMock),
      instance(ContextBuilderMock)
    );
  });

  it("should be defined", () => {
    expect(true).toBe(true);
  });
});
