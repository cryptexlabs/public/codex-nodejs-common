import { ExecutionContext } from "@nestjs/common";
import { HttpAuthzGuardUtil } from "./http-authz.guard.util";

/**
 * Authorizes detachment of objects to another object by object id
 */
export class HttpAuthzListSubObjectsGuardUtil {
  private _util: HttpAuthzGuardUtil;

  constructor(private readonly context: ExecutionContext) {
    this._util = new HttpAuthzGuardUtil(context);
  }

  /**
   * @param {string} object The object name of object A
   * @param {string} objectId The object ID of object A
   * @param {string} detachObject The object name of objects B
   * @param {string?} namespace (Optional) The namespace of objects A and B
   */
  public isAuthorized(
    object: string,
    objectId: string,
    subObject: string,
    namespace?: string
  ) {
    let requests = [];

    if (namespace) {
      requests = [
        {
          action: "",
          object: namespace,
          objectId: "",
        },
      ];
    }

    requests = [
      ...requests,
      {
        action: "",
        object,
        objectId,
      },
      {
        action: "list",
        object: subObject,
        objectId: "",
      },
    ];

    return this._util.isAuthorized(...requests);
  }

  public get params() {
    return this._util.params;
  }

  public get query() {
    return this._util.query;
  }

  public get body() {
    return this._util.body;
  }
}
