import { ExecutionContext, HttpException, HttpStatus } from "@nestjs/common";
import * as jwt from "jsonwebtoken";
import { AuthorizationRequestInterface } from "./authorization-request.interface";
import { AuthorizationAllowance } from "./authorization-allowance";

export class HttpAuthzGuardUtil {
  private _token: any;
  public readonly params: any;
  public readonly query: any;
  public readonly body: any;

  constructor(private readonly context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    const authorizationHeader = request.headers.authorization;
    if (!authorizationHeader) {
      throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);
    }
    const bearerTokenParts = authorizationHeader.trim().split("Bearer ");
    if (bearerTokenParts.length !== 2) {
      throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);
    }

    const bearerToken = bearerTokenParts[1];
    const decodedToken = (jwt.decode(bearerToken) as unknown) as any;
    if (!decodedToken) {
      throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);
    }

    this._token = decodedToken;
    this.params = request.params;
    this.query = request.query;
    this.body = request.body;
  }

  public isAuthorized(...authzRequests: AuthorizationRequestInterface[]) {
    const scopes = this._token.scopes;

    for (const scope of scopes) {
      if (this._doesScopeAuthorizeRequest(scope, authzRequests)) {
        return true;
      }
    }

    return false;
  }

  private _doesScopeAuthorizeRequest(
    scope: string,
    authzRequests: AuthorizationRequestInterface[]
  ): boolean {
    const parts = scope.split(":");

    const authorizationAllowances = [];
    for (let i = 0; i < parts.length; i += 3) {
      authorizationAllowances.push(
        new AuthorizationAllowance(
          this._token.sub,
          parts[i],
          parts[1 + i],
          parts[2 + i]
        )
      );
    }

    for (let i = 0; i < authzRequests.length; i++) {
      const request = authzRequests[i];
      if (!authorizationAllowances[i]) {
        return false;
      }
      const allowance = authorizationAllowances[i];
      if (!allowance.isRequestAllowed(request)) {
        return false;
      }
    }

    return true;
  }
}
