import { AuthorizationRequestInterface } from "./authorization-request.interface";

export class AuthorizationAllowance {
  constructor(
    private readonly subject,
    private readonly object,
    private readonly objectId,
    private readonly action
  ) {}

  public isRequestAllowed(request: AuthorizationRequestInterface): boolean {
    if (
      request.object &&
      request.object.trim() !== "" &&
      this.object &&
      this.object.trim() !== ""
    ) {
      // Check object
      if (this.object !== "any" && request.object !== this.object) {
        return false;
      }
    }

    // Check object id
    if (
      request.objectId &&
      request.objectId.toString().trim() !== "" &&
      this.objectId &&
      this.objectId.toString().trim() !== ""
    ) {
      if (
        this.objectId !== "any" &&
        request.objectId.toString() !== this.objectId.toString()
      ) {
        return false;
      }
    }

    if (
      request.action &&
      request.action.trim() !== "" &&
      this.action &&
      this.action !== ""
    ) {
      // Check action
      if (this.action !== "any" && request.action !== this.action) {
        return false;
      }
    }

    return true;
  }
}
