import { EventHandlerInterface } from "./event.handler.interface";
import { WebsocketBroadcasterInterface } from "../service/socket/websocket-broadcaster.interface";
import { Inject, Injectable } from "@nestjs/common";

@Injectable()
export class WebsocketEventHandler implements EventHandlerInterface {
  constructor(
    @Inject("BROADCASTER")
    private readonly broadcaster: WebsocketBroadcasterInterface
  ) {}

  async handle(message: any, topic?: string): Promise<void> {
    this.broadcaster.broadcast(topic, message);
  }
}
