import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from "@nestjs/common";
import { Observable } from "rxjs";
import { HttpResponse } from "../response";
import { tap } from "rxjs/operators";

@Injectable()
export class HttpStatusInterceptor implements NestInterceptor {
  intercept(
    context: ExecutionContext,
    next: CallHandler<any>
  ): Observable<HttpResponse<any>> {
    const response = context.switchToHttp().getResponse();
    return next.handle().pipe(
      tap((message: HttpResponse<any>) => {
        if (message && message.meta && message.meta.status) {
          response.status(message.meta.status);
        }
      })
    );
  }
}
