export class NotImplementedError extends Error {
  private constructor(clazzMethod) {
    super(`${clazzMethod} not implemented`);
  }

  public static throw<T>(): T {
    throw this.make();
  }

  public static make() {
    try {
      throw new Error();
    } catch (e) {
      const stack = e.stack || "";
      const stackLines = stack.split("\n");
      const callerLine = stackLines[3];
      const match = callerLine.match(/at (\S+)/);
      if (match && match.length > 1) {
        return new NotImplementedError(match[1]);
      } else {
        return new NotImplementedError("Unknown");
      }
    }
  }
}
