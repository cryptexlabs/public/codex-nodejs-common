import { Injectable, LoggerService } from "@nestjs/common";
import {
  ApiMetaHeadersInterface,
  ClientInterface,
  Locale,
  LocaleI18nInterface,
  LocaleInterface,
  MessageContextInterface,
  MessageMetaInterface,
} from "@cryptexlabs/codex-data-model";
import { DefaultConfig } from "../config";
import { Context } from "./context";
import { v4 as uuidv4 } from "uuid";
import { LocaleUtil } from "../util";
import { I18n } from "i18n";

@Injectable()
export class ContextBuilder {
  private locale: LocaleI18nInterface;
  private correlationId: string;
  private started: Date;
  private _path?: string;
  private _now?: Date;

  constructor(
    private readonly logger: LoggerService,
    private readonly config: DefaultConfig,
    public client: ClientInterface,
    private readonly messageContext: MessageContextInterface,
    private i18nData?: i18nAPI
  ) {}

  public now(): Date {
    if (this._now) {
      return this._now;
    }
    return new Date();
  }

  public setNow(now: Date) {
    this._now = now;
  }

  public build() {
    return new ContextBuilder(
      this.logger,
      this.config,
      {
        name: this.client.name,
        version: this.client.version,
        id: this.client.id,
        variant: this.client.variant,
      },
      {
        category: this.messageContext.category,
        id: this.messageContext.id,
      },
      this.i18nData
    );
  }

  public setI18nData(i18nData: i18nAPI) {
    this.i18nData = i18nData;
    return this;
  }

  public setMetaFromHeaders(
    headers: ApiMetaHeadersInterface & { "x-forwarded-uri"?: string }
  ) {
    this.setMetaFromHeadersForNewMessage(headers);
    if (headers["x-client-version"]) {
      this.client.version = headers["x-client-version"];
    }
    if (headers["x-client-id"]) {
      this.client.id = headers["x-client-id"];
    }
    if (headers["x-client-name"]) {
      this.client.name = headers["x-client-name"];
    }
    if (headers["x-client-variant"]) {
      this.client.variant = headers["x-client-variant"];
    }
    if (headers["x-forwarded-uri"]) {
      this._path = headers["x-forwarded-uri"];
    }
    if (headers["x-now"]) {
      this._now = new Date(headers["x-now"]);
    }

    return this;
  }

  public setMetaFromHeadersForNewMessage(
    headers: ApiMetaHeadersInterface & { "x-forwarded-uri"?: string }
  ) {
    this.setCorrelationId(headers["x-correlation-id"]);
    this.setLocale(LocaleUtil.getLocaleFromHeaders(headers, this.getResult()));
    this.setStarted(headers["x-started"]);
    if (headers["x-context-category"]) {
      this.messageContext.category = headers["x-context-category"];
    }
    if (headers["x-context-id"]) {
      this.messageContext.id = headers["x-context-id"];
    }
    if (headers["x-forwarded-uri"]) {
      this._path = headers["x-forwarded-uri"];
    }
    return this;
  }

  public setMeta(meta: MessageMetaInterface) {
    this.setCorrelationId(meta.correlationId);
    this.setLocale(new Locale(meta.locale.language, meta.locale.country));
    this.setStarted(meta.time.started);
    return this;
  }

  public setCorrelationId(correlationId: string): ContextBuilder {
    this.correlationId = correlationId;
    return this;
  }

  public setLocale(locale: LocaleInterface) {
    this.locale = new Locale(locale.language, locale.country);
    return this;
  }

  public setStarted(date: Date | string) {
    if (typeof date === "string") {
      this.started = new Date(date);
    } else {
      this.started = date;
    }
    return this;
  }

  private _getStarted(): Date {
    return this.started || new Date();
  }

  private _getCorrelationId(): string {
    return this.correlationId || uuidv4();
  }

  private _getLocale(): LocaleI18nInterface {
    return this.locale || new Locale("en", "US");
  }

  public getResult(): Context {
    const i18nInstance = new I18n();
    const local = this._getLocale();
    i18nInstance.configure({
      locales: [local.i18n],
      staticCatalog: this.i18nData.getCatalog(),
      defaultLocale: local.i18n,
    });

    const context = new Context(
      this._getCorrelationId(),
      this.logger,
      this.config,
      this.client,
      this._getLocale(),
      this.messageContext,
      this._getStarted(),
      i18nInstance,
      this._path
    );

    if (this._now) {
      context.setNow(this._now);
    }

    return context;
  }
}
