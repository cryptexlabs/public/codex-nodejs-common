import { ErrorMessageInterface } from "@cryptexlabs/codex-data-model";

export interface ErrorMessageDataInterface {
  message: ErrorMessageInterface;
  stack: string;
}
