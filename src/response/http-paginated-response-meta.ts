import { HttpStatus } from "@nestjs/common";
import {
  LocaleInterface,
  MessageMetaInterface,
} from "@cryptexlabs/codex-data-model";
import { DefaultConfig } from "../config";
import { MessageMeta } from "../message";

export class HttpPaginatedResponseMeta
  extends MessageMeta
  implements MessageMetaInterface {
  constructor(
    public readonly status: HttpStatus,
    public readonly totalRecords: number,
    type: string,
    locale: LocaleInterface,
    config: DefaultConfig,
    correlationId: string,
    started: Date,
    public readonly path?: string
  ) {
    super(type, locale, config, correlationId, started);
  }

  public toJSON(): any {
    return {
      totalRecords: this.totalRecords,
      status: this.status,
      path: this.path,
      type: this.type,
      schemaVersion: this.schemaVersion,
      correlationId: this.correlationId,
      time: this.time,
      context: this.context,
      locale: this.locale,
      client: this.client,
    };
  }
}
