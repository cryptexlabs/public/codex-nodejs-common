import { ContextualHttpResponse } from "./contextual-http.response";
import { Context } from "../context";
import { HttpStatus } from "@nestjs/common";
import { JsonSerializableInterface } from "../message";

export class SimpleHttpResponse
  extends ContextualHttpResponse<string>
  implements JsonSerializableInterface<any> {
  constructor(context: Context, httpStatus: HttpStatus, phraseKey: string) {
    super(context, httpStatus, context.i18n, phraseKey);
  }
}
