import { ElasticsearchService } from "@nestjs/elasticsearch";
import { CustomLogger } from "../../logger";
import { ElasticsearchPinger } from "./elasticsearch.pinger";

export class ElasticsearchInitializer {
  private _indexInitialized: boolean = false;

  constructor(
    private readonly logger: CustomLogger,
    private readonly index: string,
    private readonly elasticsearchService: ElasticsearchService,
    private readonly indexProperties: any,

    private readonly elasticsearchPinger: ElasticsearchPinger,

    private readonly type?: string
  ) {}

  private async _createIndex() {
    await this.elasticsearchService.indices.create({
      index: this.index,
      body: {},
    });
  }

  private async _getIndexExists() {
    return await this.elasticsearchService.indices.exists({
      index: this.index,
    });
  }

  async initializeIndex() {
    if (this._indexInitialized) {
      return;
    }
    this._indexInitialized = true;
    await this.elasticsearchPinger.waitForReady();

    const exists = await this._getIndexExists();
    if (!exists) {
      await this._createIndex();
    }

    let params: any = {
      index: this.index,
      body: {
        properties: this.indexProperties,
      },
    };
    if (this.type) {
      params = {
        ...params,
        type: this.type,
      };
    }
    await this.elasticsearchService.indices.putMapping(params);
  }
}
