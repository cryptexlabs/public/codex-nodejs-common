import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  registerDecorator,
  ValidationOptions,
} from "class-validator";
import { EmailTypeEnum, PhoneTypeEnum } from "@cryptexlabs/authf-data-model";

@ValidatorConstraint({ name: "IsValidPhoneType", async: false })
export class IsValidPhoneTypeConstraint
  implements ValidatorConstraintInterface {
  private readonly PHONE_TYPES: string[] = Object.values(PhoneTypeEnum);

  validate(timezone: any, args: ValidationArguments): boolean {
    return this.PHONE_TYPES.includes(timezone);
  }

  defaultMessage(args: ValidationArguments): string {
    return `"${args.value}" is not a valid UTC timezone. Choose one of the allowed timezones.`;
  }
}

export function IsValidPhoneType(validationOptions?: ValidationOptions) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsValidPhoneTypeConstraint,
    });
  };
}
