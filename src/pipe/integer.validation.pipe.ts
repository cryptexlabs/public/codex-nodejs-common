import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from "@nestjs/common";

@Injectable()
export class IntegerValidationPipe implements PipeTransform {
  public transform(value: any, metadata: ArgumentMetadata): any {
    if (parseInt(value, 10).toString() !== value.toString()) {
      throw new BadRequestException(`${value} must be an integer`);
    }
    return value;
  }
}
