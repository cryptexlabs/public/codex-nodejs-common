import { CodexMetaTypeEnum } from "@cryptexlabs/codex-data-model";

export const ExtendedCodexMetaType = {
  ...CodexMetaTypeEnum,

  get MY_TYPE(): CodexMetaTypeEnum {
    return "extended.my-type" as CodexMetaTypeEnum;
  },
};
