import { WebsocketClientMessageTypeEnum } from "./websocket.client.message.type.enum";
export interface WebsocketClientMessageInterface {
    meta: {
        type: WebsocketClientMessageTypeEnum;
    };
    data: any;
}
