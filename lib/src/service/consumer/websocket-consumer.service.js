"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebsocketConsumerService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../../config");
const jsYaml = require("js-yaml");
const fs = require("fs");
const event_handler_1 = require("../../event/event-handler");
const consumer_util_1 = require("./consumer.util");
const healthz_1 = require("../healthz");
const socket_1 = require("../socket");
const randomstring = require("randomstring");
let WebsocketConsumerService = class WebsocketConsumerService {
    constructor(healthzService, consumerDelegateService, config, logger, internalFactory, broadcasterService) {
        this.healthzService = healthzService;
        this.consumerDelegateService = consumerDelegateService;
        this.config = config;
        this.logger = logger;
        this.internalFactory = internalFactory;
        this.broadcasterService = broadcasterService;
        this.topics = jsYaml.safeLoad(fs.readFileSync(config.kafka.topicsConfigPath).toString());
        this.eventHandler = new event_handler_1.EventHandler(logger);
    }
    async init() {
        await this.healthzService.makeUnhealthy(healthz_1.HealthzComponentEnum.MESSAGE_BROKER_KAFKA);
        await this.consumerDelegateService.initializeConsumer(randomstring.generate());
        await this.consumerDelegateService.connect();
        await this.healthzService.makeHealthy(healthz_1.HealthzComponentEnum.MESSAGE_BROKER_KAFKA);
    }
    async restart(topics) {
        try {
            this.logger.verbose(`Stopping consumer`);
            await this.consumerDelegateService.stopConsumer();
            this.logger.verbose(`Consumer stopped`);
            const finalTopics = consumer_util_1.ConsumerUtil.getTopics(topics);
            this.logger.verbose(`Final topics`, { finalTopics });
            await this.consumerDelegateService.startConsumer(finalTopics, async (topic, message) => {
                var _a, _b;
                try {
                    const body = JSON.parse(message.value.toString());
                    const messageTopic = (_b = (_a = body.meta) === null || _a === void 0 ? void 0 : _a.type) !== null && _b !== void 0 ? _b : topic;
                    this.broadcasterService.broadcast(messageTopic, body);
                }
                catch (e) {
                    this.logger.error(e);
                }
            });
        }
        catch (e) {
            await this.healthzService.makeUnhealthy(healthz_1.HealthzComponentEnum.MESSAGE_BROKER_KAFKA);
        }
    }
};
exports.WebsocketConsumerService = WebsocketConsumerService;
exports.WebsocketConsumerService = WebsocketConsumerService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)("HEALTHZ_SERVICE")),
    __param(1, (0, common_1.Inject)("CONSUMER_SERVICE_DELEGATE")),
    __param(2, (0, common_1.Inject)("CONFIG")),
    __param(3, (0, common_1.Inject)("LOGGER")),
    __param(4, (0, common_1.Inject)("INTERNAL_DOMAIN_EVENT_HANDLER_FACTORY")),
    __param(5, (0, common_1.Inject)("BROADCASTER")),
    __metadata("design:paramtypes", [healthz_1.HealthzService, Object, config_1.DefaultConfig, Object, Object, socket_1.BroadcasterService])
], WebsocketConsumerService);
//# sourceMappingURL=websocket-consumer.service.js.map