export interface ConsumerServiceDelegateInterface {
    connect(): Promise<void>;
    startConsumer(topics: (string | RegExp)[], callback: (topic: string, message: any) => Promise<void>, err?: (error: string) => Promise<void>): Promise<void>;
    initializeConsumer(consumerGroup: string, config?: any): Promise<void>;
    stopConsumer(): Promise<void>;
    initializeProducer(): Promise<void>;
    publish(topic: string, payload: any): Promise<void>;
}
