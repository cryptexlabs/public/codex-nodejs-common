import { ErrorMessageInterface, LocaleI18nInterface } from "@cryptexlabs/codex-data-model";
import { JsonSerializableInterface } from "../message";
export declare class ErrorMessage implements ErrorMessageInterface, JsonSerializableInterface<ErrorMessageInterface> {
    private readonly _developerMessage;
    private readonly _userMessage;
    constructor(locale: LocaleI18nInterface, i18nData: i18nAPI, developerPhrase: string, userPhrase: string, fallbackDeveloperText?: string, fallbackUserText?: string);
    get developer(): string;
    get user(): string;
    toJSON(): ErrorMessageInterface;
}
