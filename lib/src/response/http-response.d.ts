import { HttpStatus } from "@nestjs/common";
import { LocaleInterface, MessageInterface } from "@cryptexlabs/codex-data-model";
import { DefaultConfig } from "../config";
import { JsonSerializableInterface } from "../message";
import { HttpResponseMeta } from "./http-response-meta";
export declare abstract class HttpResponse<T> implements JsonSerializableInterface<MessageInterface<T>>, MessageInterface<T> {
    private status;
    private type;
    meta: HttpResponseMeta;
    abstract get data(): any;
    protected constructor(status: HttpStatus, type: string, locale: LocaleInterface, config: DefaultConfig, correlationId?: string, started?: Date, path?: string);
    toJSON(): MessageInterface<T>;
}
