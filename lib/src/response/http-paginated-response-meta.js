"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpPaginatedResponseMeta = void 0;
const message_1 = require("../message");
class HttpPaginatedResponseMeta extends message_1.MessageMeta {
    constructor(status, totalRecords, type, locale, config, correlationId, started, path) {
        super(type, locale, config, correlationId, started);
        this.status = status;
        this.totalRecords = totalRecords;
        this.path = path;
    }
    toJSON() {
        return {
            totalRecords: this.totalRecords,
            status: this.status,
            path: this.path,
            type: this.type,
            schemaVersion: this.schemaVersion,
            correlationId: this.correlationId,
            time: this.time,
            context: this.context,
            locale: this.locale,
            client: this.client,
        };
    }
}
exports.HttpPaginatedResponseMeta = HttpPaginatedResponseMeta;
//# sourceMappingURL=http-paginated-response-meta.js.map