"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StringUtil = void 0;
const crypto = require("crypto");
class StringUtil {
    constructor() { }
    static isRegexString(str) {
        return str.substr(0, 1) === "/" && str.substr(str.length - 1, 1) === "/";
    }
    static stringMatches(test, str) {
        const testTrimmed = test.trim();
        const strTrimmed = str.trim();
        if (test.trim() === str.trim()) {
            return true;
        }
        if (this.isRegexString(testTrimmed)) {
            if (new RegExp(testTrimmed.substr(1, testTrimmed.length - 2)).test(strTrimmed)) {
                return true;
            }
        }
        return false;
    }
    static insecureMd5(str) {
        return crypto.createHash("md5").update(str).digest("hex");
    }
    static insecureSha1(str) {
        return crypto.createHash("sha1").update(str).digest("hex");
    }
    static sha256(str) {
        return crypto.createHash("sha256").update(str).digest("hex");
    }
    static sha512(str) {
        return crypto.createHash("sha512").update(str).digest("hex");
    }
    static byteArray(str) {
        return [...Buffer.from(str)];
    }
    static isValidUUIDv4(uuid) {
        const regex = /^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
        return regex.test(uuid);
    }
}
exports.StringUtil = StringUtil;
//# sourceMappingURL=string.util.js.map