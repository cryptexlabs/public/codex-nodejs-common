import { I18n } from "i18n";
declare const i18nData: i18nAPI;
declare const i18nInstance: I18n;
export { i18nData, i18nInstance as i18n };
