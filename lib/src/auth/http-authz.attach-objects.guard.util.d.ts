import { ExecutionContext } from "@nestjs/common";
export declare class HttpAuthzAttachObjectsGuardUtil {
    private readonly context;
    private _util;
    constructor(context: ExecutionContext);
    isAuthorized(object: string, objectId: string, attachObject: string, attachObjectIds: string[], namespace?: string): boolean;
    get params(): any;
    get query(): any;
    get body(): any;
}
