import { AuthenticatorInterface } from "./authenticator.interface";
export declare class FakeAuthenticator implements AuthenticatorInterface {
    authenticate(token: string): Promise<boolean>;
}
