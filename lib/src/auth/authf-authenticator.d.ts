import { AuthenticatorInterface } from "./authenticator.interface";
import { ClientInterface } from "@cryptexlabs/codex-data-model";
import { AuthfAuthenticatorOptionsInterface } from "./authf-authenticator.options.interface";
export declare class AuthfAuthenticator implements AuthenticatorInterface {
    private readonly authfUrl;
    private readonly client;
    private readonly options?;
    constructor(authfUrl: string, client: ClientInterface, options?: AuthfAuthenticatorOptionsInterface);
    authenticate(token: string): Promise<boolean>;
}
