import { ExecutionContext } from "@nestjs/common";
export declare class HttpAuthzActionToSubObjectsGuardUtil {
    private readonly context;
    private readonly action;
    private _authzGuard;
    constructor(context: ExecutionContext, action: string);
    isAuthorized(object: string, objectId: string, subObject: string, subObjectIds: string[], namespace?: string): boolean;
    get params(): any;
    get query(): any;
    get body(): any;
}
