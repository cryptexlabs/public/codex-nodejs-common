import { ClientInterface, LocaleInterface, MessageContextInterface, MessageMetaInterface, MetaTimeInterface } from "@cryptexlabs/codex-data-model";
import { DefaultConfig } from "../config";
import { JsonSerializableInterface } from "./json-serializable.interface";
export declare class MessageMeta implements MessageMetaInterface, JsonSerializableInterface<MessageMetaInterface> {
    readonly type: string;
    readonly locale: LocaleInterface;
    private readonly started?;
    time: MetaTimeInterface;
    context: MessageContextInterface;
    interactionId: string | null;
    schemaVersion: "0.1.0";
    private _correlationId;
    get correlationId(): string;
    client: ClientInterface;
    constructor(type: string, locale: LocaleInterface, config: DefaultConfig, correlationId?: string, started?: Date);
    toJSON(): MessageMetaInterface;
}
