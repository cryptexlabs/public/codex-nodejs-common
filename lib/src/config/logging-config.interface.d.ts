export type LOG_LEVEL = "fatal" | "error" | "warn" | "info" | "debug" | "trace" | "verbose";
export declare const DEFAULT_LOG_LEVELS: {
    [key in LOG_LEVEL]: number;
};
