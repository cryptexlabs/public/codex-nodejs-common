import { UrlInterface } from "./url.interface";
import { PingConfigInterface } from "./ping-config.interface";
export declare class ElasticsearchConfig implements UrlInterface, PingConfigInterface {
    readonly url: string;
    readonly pingIntervalSeconds: number;
    constructor(url: string, pingIntervalSeconds: string);
}
