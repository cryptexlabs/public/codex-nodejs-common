"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticatedConnectionConfig = void 0;
const connection_config_1 = require("./connection-config");
class AuthenticatedConnectionConfig extends connection_config_1.ConnectionConfig {
    get connectionUrl() {
        return `${this.protocol}://${this.username}:${this.password}@${this.endpoint}`;
    }
}
exports.AuthenticatedConnectionConfig = AuthenticatedConnectionConfig;
//# sourceMappingURL=authenticated-connection-config.js.map