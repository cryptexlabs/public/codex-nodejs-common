import { SwitchConfigInterface } from "./switch-config.interface";
import { HostConfigInterface } from "./host-config.interface";
import { HostConfig } from "./host-config";
export declare class MetricsHostConfig extends HostConfig implements SwitchConfigInterface, HostConfigInterface {
    readonly host: any;
    readonly enabled: boolean;
    constructor(host: any, port: string, enabled: string);
}
