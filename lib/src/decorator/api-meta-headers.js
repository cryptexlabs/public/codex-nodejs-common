"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiMetaHeaders = ApiMetaHeaders;
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const uuid_1 = require("uuid");
function ApiMetaHeaders() {
    return (0, common_1.applyDecorators)(...[
        (0, swagger_1.ApiHeader)({
            name: "X-Correlation-Id",
            required: true,
            schema: {
                example: (0, uuid_1.v4)(),
            },
        }),
        (0, swagger_1.ApiHeader)({
            name: "Accept-Language",
            required: true,
            schema: {
                example: "en-US",
            },
        }),
        (0, swagger_1.ApiHeader)({
            name: "X-Started",
            description: "The time that the very first user initiated or time initiated event that eventually lead to this api being called was created",
            required: false,
            schema: {
                example: new Date().toISOString(),
            },
        }),
        (0, swagger_1.ApiHeader)({
            name: "X-Context-Category",
            description: "A category for the context of the request. For example 'test' or 'performance test'",
            required: false,
            schema: {
                example: "default",
            },
        }),
        (0, swagger_1.ApiHeader)({
            name: "X-Context-Id",
            description: "A unique context identifier used for correlating logs and metrics usually for performance or experimental testing",
            required: false,
            schema: {
                example: "none",
            },
        }),
        (0, swagger_1.ApiHeader)({
            name: "X-Client-Id",
            description: "A unique identifier for the client to help identify exactly which application or third party is making the requests",
            required: false,
            schema: {
                example: "dafae812-6a07-4e4f-8ac5-d5a81bb32cab",
            },
        }),
        (0, swagger_1.ApiHeader)({
            name: "X-Client-Name",
            description: "The name of the client",
            required: false,
            schema: {
                example: "swagger",
            },
        }),
        (0, swagger_1.ApiHeader)({
            name: "X-Client-Version",
            description: "The version of the client. If the client is making bad requests this helps " +
                "identify which version of the client is making the bad requests",
            required: false,
            schema: {
                example: "0.3.6",
            },
        }),
        (0, swagger_1.ApiHeader)({
            name: "X-Client-Variant",
            description: "Usually an environment or build type such as dev, test, or prod",
            required: false,
            schema: {
                example: "dev",
            },
        }),
        (0, swagger_1.ApiHeader)({
            name: "X-Created",
            description: "The time the client initiated the http request",
            required: false,
            schema: {
                example: new Date().toISOString(),
            },
        }),
        (0, swagger_1.ApiHeader)({
            name: "X-Now",
            description: "Override the current time for testing purposes",
            required: false,
            schema: {
                example: new Date().toISOString(),
            },
        }),
    ]);
}
//# sourceMappingURL=api-meta-headers.js.map