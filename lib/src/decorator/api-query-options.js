"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiQueryOptions = ApiQueryOptions;
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
function ApiQueryOptions() {
    return (0, common_1.applyDecorators)(...[
        (0, swagger_1.ApiQuery)({
            name: "orderBy",
            required: false,
            schema: {
                example: "name",
            },
        }),
        (0, swagger_1.ApiQuery)({
            name: "orderDirection",
            required: false,
            schema: {
                example: "asc",
            },
        }),
        (0, swagger_1.ApiQuery)({
            name: "searchName",
            required: false,
        }),
        (0, swagger_1.ApiQuery)({
            name: "search",
            required: false,
        }),
    ]);
}
//# sourceMappingURL=api-query-options.js.map