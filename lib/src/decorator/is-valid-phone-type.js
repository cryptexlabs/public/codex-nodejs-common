"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsValidPhoneTypeConstraint = void 0;
exports.IsValidPhoneType = IsValidPhoneType;
const class_validator_1 = require("class-validator");
const authf_data_model_1 = require("@cryptexlabs/authf-data-model");
let IsValidPhoneTypeConstraint = class IsValidPhoneTypeConstraint {
    constructor() {
        this.PHONE_TYPES = Object.values(authf_data_model_1.PhoneTypeEnum);
    }
    validate(timezone, args) {
        return this.PHONE_TYPES.includes(timezone);
    }
    defaultMessage(args) {
        return `"${args.value}" is not a valid UTC timezone. Choose one of the allowed timezones.`;
    }
};
exports.IsValidPhoneTypeConstraint = IsValidPhoneTypeConstraint;
exports.IsValidPhoneTypeConstraint = IsValidPhoneTypeConstraint = __decorate([
    (0, class_validator_1.ValidatorConstraint)({ name: "IsValidPhoneType", async: false })
], IsValidPhoneTypeConstraint);
function IsValidPhoneType(validationOptions) {
    return (object, propertyName) => {
        (0, class_validator_1.registerDecorator)({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsValidPhoneTypeConstraint,
        });
    };
}
//# sourceMappingURL=is-valid-phone-type.js.map