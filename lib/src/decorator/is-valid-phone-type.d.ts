import { ValidatorConstraintInterface, ValidationArguments, ValidationOptions } from "class-validator";
export declare class IsValidPhoneTypeConstraint implements ValidatorConstraintInterface {
    private readonly PHONE_TYPES;
    validate(timezone: any, args: ValidationArguments): boolean;
    defaultMessage(args: ValidationArguments): string;
}
export declare function IsValidPhoneType(validationOptions?: ValidationOptions): (object: any, propertyName: string) => void;
