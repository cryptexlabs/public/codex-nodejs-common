"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Uuidv4ValidationPipe = void 0;
const common_1 = require("@nestjs/common");
const Joi = require("joi");
let Uuidv4ValidationPipe = class Uuidv4ValidationPipe {
    constructor() {
        this._schema = Joi.string().uuid({ version: "uuidv4" });
    }
    transform(value, metadata) {
        const { error } = this._schema.validate(value.data);
        if (error) {
            throw new common_1.BadRequestException(error.message);
        }
        return value;
    }
};
exports.Uuidv4ValidationPipe = Uuidv4ValidationPipe;
exports.Uuidv4ValidationPipe = Uuidv4ValidationPipe = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [])
], Uuidv4ValidationPipe);
//# sourceMappingURL=uuidv4.validation.pipe.js.map