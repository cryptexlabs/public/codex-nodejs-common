import { ArgumentMetadata, PipeTransform } from "@nestjs/common";
export declare class Uuidv4ArrayValidationPipe implements PipeTransform {
    private _schema;
    constructor();
    transform(value: any, metadata: ArgumentMetadata): any;
}
