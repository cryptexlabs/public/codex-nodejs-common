import { ArgumentMetadata, PipeTransform } from "@nestjs/common";
export declare class Uuidv4ValidationPipe implements PipeTransform {
    private _schema;
    constructor();
    transform(value: any, metadata: ArgumentMetadata): any;
}
