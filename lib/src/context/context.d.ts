import { ClientInterface, LocaleI18nInterface, MessageContextInterface, MessageMetaInterface } from "@cryptexlabs/codex-data-model";
import { LoggerService } from "@nestjs/common";
import { DefaultConfig } from "../config";
import { I18n } from "i18n";
export declare class Context {
    correlationId: string;
    config: DefaultConfig;
    client: ClientInterface;
    readonly locale: LocaleI18nInterface;
    readonly messageContext: MessageContextInterface;
    readonly i18n?: I18n;
    readonly path?: string;
    readonly logger: LoggerService;
    started: Date;
    private _now;
    constructor(correlationId: string, logger: LoggerService, config: DefaultConfig, client: ClientInterface, locale: LocaleI18nInterface, messageContext: MessageContextInterface, started?: Date, i18n?: I18n, path?: string);
    now(): Date;
    setNow(now: Date): void;
    getMessageMeta(type: string): MessageMetaInterface;
}
