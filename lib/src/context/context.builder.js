"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ContextBuilder_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContextBuilder = void 0;
const common_1 = require("@nestjs/common");
const codex_data_model_1 = require("@cryptexlabs/codex-data-model");
const config_1 = require("../config");
const context_1 = require("./context");
const uuid_1 = require("uuid");
const util_1 = require("../util");
const i18n_1 = require("i18n");
let ContextBuilder = ContextBuilder_1 = class ContextBuilder {
    constructor(logger, config, client, messageContext, i18nData) {
        this.logger = logger;
        this.config = config;
        this.client = client;
        this.messageContext = messageContext;
        this.i18nData = i18nData;
    }
    now() {
        if (this._now) {
            return this._now;
        }
        return new Date();
    }
    setNow(now) {
        this._now = now;
    }
    build() {
        return new ContextBuilder_1(this.logger, this.config, {
            name: this.client.name,
            version: this.client.version,
            id: this.client.id,
            variant: this.client.variant,
        }, {
            category: this.messageContext.category,
            id: this.messageContext.id,
        }, this.i18nData);
    }
    setI18nData(i18nData) {
        this.i18nData = i18nData;
        return this;
    }
    setMetaFromHeaders(headers) {
        this.setMetaFromHeadersForNewMessage(headers);
        if (headers["x-client-version"]) {
            this.client.version = headers["x-client-version"];
        }
        if (headers["x-client-id"]) {
            this.client.id = headers["x-client-id"];
        }
        if (headers["x-client-name"]) {
            this.client.name = headers["x-client-name"];
        }
        if (headers["x-client-variant"]) {
            this.client.variant = headers["x-client-variant"];
        }
        if (headers["x-forwarded-uri"]) {
            this._path = headers["x-forwarded-uri"];
        }
        if (headers["x-now"]) {
            this._now = new Date(headers["x-now"]);
        }
        return this;
    }
    setMetaFromHeadersForNewMessage(headers) {
        this.setCorrelationId(headers["x-correlation-id"]);
        this.setLocale(util_1.LocaleUtil.getLocaleFromHeaders(headers, this.getResult()));
        this.setStarted(headers["x-started"]);
        if (headers["x-context-category"]) {
            this.messageContext.category = headers["x-context-category"];
        }
        if (headers["x-context-id"]) {
            this.messageContext.id = headers["x-context-id"];
        }
        if (headers["x-forwarded-uri"]) {
            this._path = headers["x-forwarded-uri"];
        }
        return this;
    }
    setMeta(meta) {
        this.setCorrelationId(meta.correlationId);
        this.setLocale(new codex_data_model_1.Locale(meta.locale.language, meta.locale.country));
        this.setStarted(meta.time.started);
        return this;
    }
    setCorrelationId(correlationId) {
        this.correlationId = correlationId;
        return this;
    }
    setLocale(locale) {
        this.locale = new codex_data_model_1.Locale(locale.language, locale.country);
        return this;
    }
    setStarted(date) {
        if (typeof date === "string") {
            this.started = new Date(date);
        }
        else {
            this.started = date;
        }
        return this;
    }
    _getStarted() {
        return this.started || new Date();
    }
    _getCorrelationId() {
        return this.correlationId || (0, uuid_1.v4)();
    }
    _getLocale() {
        return this.locale || new codex_data_model_1.Locale("en", "US");
    }
    getResult() {
        const i18nInstance = new i18n_1.I18n();
        const local = this._getLocale();
        i18nInstance.configure({
            locales: [local.i18n],
            staticCatalog: this.i18nData.getCatalog(),
            defaultLocale: local.i18n,
        });
        const context = new context_1.Context(this._getCorrelationId(), this.logger, this.config, this.client, this._getLocale(), this.messageContext, this._getStarted(), i18nInstance, this._path);
        if (this._now) {
            context.setNow(this._now);
        }
        return context;
    }
};
exports.ContextBuilder = ContextBuilder;
exports.ContextBuilder = ContextBuilder = ContextBuilder_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [Object, config_1.DefaultConfig,
        codex_data_model_1.ClientInterface, Object, Object])
], ContextBuilder);
//# sourceMappingURL=context.builder.js.map