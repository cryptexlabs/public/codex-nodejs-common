import { HttpException, HttpStatus } from "@nestjs/common";
import { Context } from "../context";
export declare class FriendlyHttpException extends HttpException {
    readonly context: Context;
    readonly userMessage: string;
    readonly stack: string;
    constructor(response: string | Record<string, any>, context: Context, userMessage: string, status: HttpStatus, stack: string);
}
