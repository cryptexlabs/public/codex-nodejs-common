"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FriendlyHttpException = void 0;
const common_1 = require("@nestjs/common");
class FriendlyHttpException extends common_1.HttpException {
    constructor(response, context, userMessage, status, stack) {
        super(response, status);
        this.context = context;
        this.userMessage = userMessage;
        this.stack = stack;
    }
}
exports.FriendlyHttpException = FriendlyHttpException;
//# sourceMappingURL=friendly-http-exception.js.map