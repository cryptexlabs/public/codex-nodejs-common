export interface EventHandlerInterface {
    handle(message: any, topic?: string): Promise<void>;
}
