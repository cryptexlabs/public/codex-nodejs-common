import { EventHandlerInterface } from "./event.handler.interface";
import { WebsocketBroadcasterInterface } from "../service/socket/websocket-broadcaster.interface";
export declare class WebsocketEventHandler implements EventHandlerInterface {
    private readonly broadcaster;
    constructor(broadcaster: WebsocketBroadcasterInterface);
    handle(message: any, topic?: string): Promise<void>;
}
