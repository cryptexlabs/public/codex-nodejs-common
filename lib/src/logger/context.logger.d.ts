import { LoggerService } from "@nestjs/common";
import { DefaultConfig } from "../config";
import { ClientInterface } from "@cryptexlabs/codex-data-model";
export declare class ContextLogger implements LoggerService {
    private readonly correlationId;
    private readonly config;
    private readonly client;
    private readonly logger;
    private readonly stackTraceId;
    constructor(correlationId: any, config: DefaultConfig, client: ClientInterface, logger: LoggerService);
    debug(message: any, data?: any): any;
    error(message: any, data?: any): any;
    log(message: any, data?: any): any;
    verbose(message: any, data?: any): any;
    warn(message: any, data?: any): any;
    private _getData;
}
