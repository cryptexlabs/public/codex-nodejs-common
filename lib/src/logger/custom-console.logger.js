"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var CustomConsoleLogger_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomConsoleLogger = void 0;
const common_1 = require("@nestjs/common");
const shared_utils_1 = require("@nestjs/common/utils/shared.utils");
const cli_colors_util_1 = require("@nestjs/common/utils/cli-colors.util");
const config_1 = require("../config");
let CustomConsoleLogger = CustomConsoleLogger_1 = class CustomConsoleLogger extends common_1.ConsoleLogger {
    constructor(config) {
        super();
        this.config = config;
    }
    printMessages(messages, context = "", logLevel = "log", writeStreamType) {
        const space = this.config.prettyPrintLogs ? 2 : 0;
        const color = this._getColorByLogLevel(logLevel);
        messages.forEach((message) => {
            const output = (0, shared_utils_1.isPlainObject)(message)
                ? `${color("Object:")}\n${JSON.stringify(message, (key, value) => typeof value === "bigint" ? value.toString() : value, space)}\n`
                : color(message);
            const pidMessage = color(`[Nest] ${process.pid}  - `);
            const contextMessage = context ? (0, cli_colors_util_1.yellow)(`[${context}] `) : "";
            const timestampDiff = this._updateAndGetTimestampDiff();
            const formattedLogLevel = color(logLevel.toUpperCase().padStart(7, " "));
            const computedMessage = `${pidMessage}${this.getTimestamp()} ${formattedLogLevel} ${contextMessage}${output}${timestampDiff}\n`;
            process[writeStreamType !== null && writeStreamType !== void 0 ? writeStreamType : "stdout"].write(computedMessage);
        });
    }
    _updateAndGetTimestampDiff() {
        var _a;
        const includeTimestamp = CustomConsoleLogger_1._lastTimestampAt && ((_a = this.options) === null || _a === void 0 ? void 0 : _a.timestamp);
        const result = includeTimestamp
            ? (0, cli_colors_util_1.yellow)(` +${Date.now() - CustomConsoleLogger_1._lastTimestampAt}ms`)
            : "";
        CustomConsoleLogger_1._lastTimestampAt = Date.now();
        return result;
    }
    _getColorByLogLevel(level) {
        switch (level) {
            case "debug":
                return cli_colors_util_1.clc.magentaBright;
            case "warn":
                return cli_colors_util_1.clc.yellow;
            case "error":
                return cli_colors_util_1.clc.red;
            case "verbose":
                return cli_colors_util_1.clc.cyanBright;
            default:
                return cli_colors_util_1.clc.green;
        }
    }
};
exports.CustomConsoleLogger = CustomConsoleLogger;
exports.CustomConsoleLogger = CustomConsoleLogger = CustomConsoleLogger_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)("CONFIG")),
    __metadata("design:paramtypes", [config_1.DefaultConfig])
], CustomConsoleLogger);
//# sourceMappingURL=custom-console.logger.js.map