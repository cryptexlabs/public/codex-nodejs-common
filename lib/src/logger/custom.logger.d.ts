import { LoggerService } from "@nestjs/common";
export declare class CustomLogger implements LoggerService {
    private defaultContext;
    private readonly logger;
    private readonly filterContext;
    constructor(defaultContext?: string, filterContext?: string[], isTimestampEnabled?: boolean, logger?: LoggerService);
    log(message: string, ...optionalParams: any): void;
    error(error: string | Error, ...optionalParams: any): void;
    debug(message: any, ...optionalParams: any): void;
    verbose(message: any, ...optionalParams: any): any;
    warn(message: any, ...optionalParams: any): any;
    info(message: any): any;
}
