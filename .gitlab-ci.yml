stages:
  - build
  - release
  - deploy

variables:
  # The prefix to add to commit messages that only include upgrades of versions
  # Due to the limitations of the workflow expressions. You must also update the workflow rules below if you change this
  # This text is matched with regular expression below. Regex characters such as `[` must be escaped!
  # For more info on the problem see: https://stackoverflow.com/questions/2933474/escape-characters-contained-by-bash-variable-in-regex-pattern
  BUMP_VERSION_MESSAGE: "[bump version]"

  INCREMENT_TYPE:
    description: "Select to release a new package version"
    value: ""
    options:
      - ""
      - "patch"
      - "minor"
      - "major"

# Only _run when there is [bump version] in the commit message and rev type is a tag or there is not [bump version] in the commit message
workflow:
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /\[bump version\]/ && $CI_COMMIT_TAG
      when: always
    - if: $CI_COMMIT_MESSAGE =~ /\[bump version\]/ && $CI_COMMIT_BRANCH
      when: never
    - if: $CI_COMMIT_MESSAGE !~ /\[bump version\]/
      when: always

.version-validate: &version-validate
  - >
    latest_tag_json=$(curl -X GET --header "Private-Token: $SHARED_CI_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/tags?per_page=1");
    echo "$latest_tag_json";
    if [[ $( echo $latest_tag_json | jq -r 'type' ) == "array" ]]; then
      latest_tag_length=$(echo "$latest_tag_json" | jq '. | length');
      latest_tag_name=;
      if (( $latest_tag_length > 0 )); then
        latest_tag_name=$(echo "$latest_tag_json" | jq -r '.[0].name');
        package_version=$(jq -r '.version' package.json);
        if [[ "$latest_tag_name" != *"$package_version"* ]]; then
          echo -e "\e[31mVersion does not match latest tag. Please change version in package.json to '$latest_tag_name'\e[39m";
          exit 1;
        fi;
      fi;
    fi;

.git-configure: &git-configure
  - echo "$GIT_SSH_PK" | base64 -d > id_rsa
  - chmod 400 id_rsa
  - mkdir -p ~/.ssh
  - cp known_hosts ~/.ssh/known_hosts
  - cp id_rsa ~/.ssh/id_rsa
  - git config --global user.name "$GITLAB_USER_NAME"
  - git config --global user.email "$GITLAB_USER_EMAIL"
  - git remote set-url origin git@$CI_SERVER_HOST:$CI_PROJECT_PATH.git

build:
  image: cryptexlabs/ts-node-ci:3.3.6
  stage: build
  allow_failure: false
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - when: always
  script:
    - yarn install
    - yarn lint
    - yarn prettier
    - yarn test
    - yarn build
  tags:
    - gitlab-org-docker

release:
  image: cryptexlabs/ts-node-ci:3.3.6
  stage: release
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $INCREMENT_TYPE && $INCREMENT_TYPE != "" && $CI_COMMIT_BRANCH == "master"
      when: on_success
    - when: never
  allow_failure: false
  script:
    - *version-validate
    - *git-configure
    - git checkout master
    - git reset --hard origin/master
    - yarn version --$INCREMENT_TYPE --no-git-tag-version --no-commit-hooks
    - version=$(jq -r '.version' package.json)
    - git add package.json
    - git commit -m "$BUMP_VERSION_MESSAGE $version"
    - git tag -d v$version || true
    - git tag v$version
    - git push origin v$version
    - git push origin master
    - >
      curl --request POST --header "Private-Token: $SHARED_CI_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/protected_tags?name=v$version"
  tags:
    - gitlab-org-docker

deploy:
  image: cryptexlabs/ts-node-ci:3.3.6
  stage: deploy
  allow_failure: false
  rules:
    - if: $CI_COMMIT_TAG
      when: on_success
    - when: never
  script:
    - echo "//registry.npmjs.org/:_authToken=$NPM_AUTH_TOKEN" > ~/.npmrc
    - version=$(jq -r '.version' package.json)
    - yarn install
    - yarn build
    - yarn publish --non-interactive --new-version $version --access public